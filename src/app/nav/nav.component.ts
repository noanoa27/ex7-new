import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from '../auth.service';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  tologin(){
    this.router.navigate(['/login']);
  }
  
  tosignup(){
    this.router.navigate(['/signup']);
  }

  towelcome(){
    this.router.navigate(['/welcome']);
  }
  logOut() {
    this.authService.logOut()
    .then(value => {
      this.router.navigate(['/login'])
    }).catch(err => {
      console.log(err)
    });
  }
 
  constructor(private router:Router,public authService:AuthService) { }

  ngOnInit() {
  }

}
